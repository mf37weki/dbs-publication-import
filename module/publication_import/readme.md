## Beispiele und Dokumentationen
jigarius Beispiel-Modul: (yeah!)
- https://github.com/jigarius/drupal-migration-example

Gute Anleitung/gutes Beispiel für ein Modul zum Importieren von JSON-Daten:
- https://git.drupalcode.org/project/migrate_plus/tree/8.x-5.x/migrate_json_example

Drupal Migrate JSON example:
- https://www.drupal.org/docs/8/api/migrate-api/migrate-destination-plugins-examples/migrating-commerce-2-product-variations

Drupal Module Development:
- https://www.drupal.org/docs/8/creating-custom-modules/

Drupal Migrate API:
- https://www.drupal.org/docs/8/api/migrate-api/executing-migrations
- https://www.drupal.org/docs/8/api/migrate-api/migrate-source-plugins/overview-of-migrate-source-plugins

Drupal SourcePluginBase:
- https://api.drupal.org/api/drupal/core%21modules%21migrate%21src%21Plugin%21migrate%21source%21SourcePluginBase.php/class/SourcePluginBase/8.4.x

EvolvingWeb Beispiel-Migration Modul: (meh!)
- https://evolvingweb.ca/blog/drupal-8-migration-migrating-basic-data-part-1
- https://evolvingweb.ca/blog/writing-custom-migration-source-plugin-drupal-8
- https://github.com/evolvingweb/migrate_example_source

## Notizen
Google Scholar-Link wird anhand der Nachnamen der Autoren + Titel der Publikation dynamisch generiert:
- http://scholar.google.com/scholar?q=+Saeedi+Peukert+Rahm+intitle%3A%22Using+Link+Features+for+Entity+Clustering+in+Knowledge+Graphs%22
- Bergami Petermann Montesi intitle:"THoSP: an Algorithm for Nesting Property Graphs"
### Pubtypes:
- paper 293
- book 20
- report 25
- poster 23
- thesis 8
- talk 1
- demo 0
- other 91
### Befehle:
- Modul installieren/aktivieren: `drush en publication_import`
- Modul deinstallieren: `drush pm:uninstall publication_import`
- Migration-Status ansehen: `drush migrate-status`
- Migration durchführen: `drush migrate-import --group=dbs`
- Migration rückgängig machen: `drush migrate-rollback --group=dbs`
- Migration stoppen: `drush migrate-stop --group=dbs`
- Migration-Status zurücksetzen: `drush migrate-reset --group=dbs`

## TODOs
- Using a contextual filter with Views to search for title instead of Node ID: https://www.drupal.org/project/entityreference/issues/2763843