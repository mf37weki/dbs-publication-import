import json
import re

publications = []
all_tags = []
all_authors = []
files = []

# First, go through the publication list and parse them into JSON
with open('data/import/publist.sql', 'r', encoding='utf-8') as file:
    pubPattern = re.compile(r'INSERT INTO publist VALUES\("(\d+)","(.*?)","(.*?)","(.*?)","(.*?)","(.*?)","(.*?)","(\d*)","(\d*)"\);', re.S)

    for pubMatch in pubPattern.finditer(file.read()):
        pubGroups = pubMatch.groups()
        authors = []
        tags = set([])
        pubdate = pubGroups[2].strip().split('-')

        # Bekannte pubdate-Formate: "2007" | "2007-01" | "2007-01-01" | "" | "2007 " | "2007-01 " | "2007-01-01 " | "2012-MM" | "2002-11-1"
        try:
            pub_year = int(pubdate[0])
        except ValueError:
            # TODO?!
            pub_year = 2000

        pub_month = None

        if len(pubdate) >= 2:
            try:
                pub_month = int(pubdate[1])
            except ValueError:
                pub_month = None
        
        if pub_month != None and pub_month > pub_year:
            year = pub_year
            pub_year = pub_month
            pub_month = year

        # Handle authors
        authorPattern = re.compile(r'([^\r\n\t\f\v ,;\(\)]+),\s*([^\r\n\t\f\v ,;\(\)]+)')

        for authorMatch in authorPattern.finditer(pubGroups[1]):
            last_name = authorMatch.group(1)
            first_name = authorMatch.group(2)
            author = last_name + ', '

            if not '.' in first_name:
                first_name = first_name[0:1] + '.'
            
            author += first_name

            authors.append(author)

            contained = False

            for a in all_authors:
                if a['name'] == author:
                    contained = True
                    break
            
            if not contained:
                all_authors.append({ 'name': author, 'first_name': first_name, 'last_name': last_name })
        
        # Handle tags
        for tag in pubGroups[3].split(','):
            for tag2 in tag.split(';'):
                tag2 = tag2.strip().lower()

                if not tag2 == '':
                    tags.add(tag2)

                    contained = False

                    for t in all_tags:
                        if t['name'] == tag2:
                            contained = True
                            break
                    
                    if not contained:
                        all_tags.append({ 'name': tag2 })
        
        url = pubGroups[4]

        if url != '' and not re.match(r'\b(([\w-]+://?|www[.])[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/)))', url):
            url = 'https://dbs.uni-leipzig.de/' + url

        publications.append({
            'id': int(pubGroups[0]),
            'title': 'MISSING TITLE #' + pubGroups[0],
            'abstract': 'MISSING ABSTRACT',
            # 'author': pubGroups[1],
            'authors': authors,
            # 'pubdate': pubGroups[2],
            'pub_year': pub_year,
            'pub_month': pub_month,
            # 'keyword': pubGroups[3],
            'tags': list(tags),
            'url': url,
            'pubtype': pubGroups[5],
            'venue': pubGroups[6],
            # 'citations': pubGroups[7],
            # 'citupdate': pubGroups[8],
            'files': []
        })

        # if len(publications) >= 5:
        #     break

# Then, go through nodes and connect them where possible with the previous created publication list
with open('data/import/node.csv', 'r', encoding='utf-8') as file:
    file_content = re.sub(r'\n\\n', r'\n', file.read())

    pubPattern = re.compile(r'(\d+)\t\|\t(.*?)\t\|\t(.*?)\t\|\t(\d+)\t\|\t(.*?)\t\|\t(.*?)\t\|\t(.*?)\t\|\t(\d+)', re.S)

    for pubMatch in pubPattern.finditer(file_content):
        pubGroups = pubMatch.groups()

        if not pubGroups[4] in pubGroups[5]:
            print('Im Falle von Publikation #' + pubGroups[0] + ' "' + pubGroups[2] + '" ist der Teaser nicht (vollständig) im Body enthalten!')

        # nid	|	type	|	title	|	uid	|	teaser	|	body	|	revisions	|	format
        for publication in publications:
            if publication['id'] == int(pubGroups[0]):
                publication['title'] = pubGroups[2].strip()
                publication['abstract'] = pubGroups[5].strip()

with open('data/import/files.sql', 'r', encoding='utf-8') as file:
    filePattern = re.compile(r'INSERT INTO files VALUES\("(\d+)","(\d+)","(.*?)","(.*?)","(.*?)","(\d+)","(\d+)"\);', re.S)

    for fileMatch in filePattern.finditer(file.read()):
        fileGroups = fileMatch.groups()

        fileid = int(fileGroups[0])
        nodeid = int(fileGroups[1])
        filename = fileGroups[2]
        filepath = fileGroups[3]
        filemime = fileGroups[4]
        filesize = int(fileGroups[5])

        # if not 'file/' + filename == filepath:
        #     print('Im Falle von File #' + str(fileId) + ' stimmt filename nicht mit filepath überein: ' + filename + ' --- ' + filepath)

        if not filepath.startswith('file/'):
            print('Im Falle von File #' + str(fileid) + ' fängt der filepath nicht mit file/ an: ' + filepath)
        
        files.append({
            'fileid': fileid,
            'nodeid': nodeid,
            'filename': filename,
            'filepath': filepath,
            'filemime': filemime,
            'filesize': filesize
        })

from os import path, makedirs
from shutil import copyfile
import os

for publication in publications:
    for file in files:
        if publication['id'] == file['nodeid']:
            publication['files'].append(file)

            importdir = 'data/import/'
            exportdir = 'data/export/'

            if path.isfile(importdir + file['filepath']):
                month = publication['pub_month']

                if month == None:
                    month = 1
                
                filedir = 'research/publications/' + str(publication['pub_year']) + '-' + str(month)

                if 'pdf' in file['filemime']:
                    filedir += '/pdf/'

                    if not path.isdir(exportdir + filedir):
                        makedirs(exportdir + filedir)

                    publication['file_pdf_destination'] = 'public://i/' + filedir

                    filedir += file['filename']
                    copyfile(importdir + file['filepath'], exportdir + filedir)

                    publication['file_pdf_source'] = 'public://import/publication/' + filedir
                elif 'image' in file['filemime']:
                    filedir += '/cover/'

                    if not path.isdir(exportdir + filedir):
                        makedirs(exportdir + filedir)

                    publication['cover_image_destination'] = 'public://i/' + filedir

                    filedir += file['filename']
                    copyfile(importdir + file['filepath'], exportdir + filedir)

                    publication['cover_image_source'] = 'public://import/publication/' + filedir
            else:
                print('Die Datei ' + file['filepath'] + ' existiert nicht')

for publication in publications:
    if len(publication['files']) > 2:
        print(str(len(publication['files'])) + ' Files: ' + str(publication['id']))
        # for file in publication['files']:
        #     print(file['filename'] + ' ' + file['filepath'] + ' ' + str(file['filesize']))
    # elif len(publication['files']) == 0:
    #     print('0 Files: ' + str(publication['id']))
# ip()

with open('data/export/publications.json', 'w+', encoding='utf-8') as file:
    json.dump({ 'publication': publications, 'tag': all_tags, 'author': all_authors }, file, ensure_ascii=False, indent=4)
